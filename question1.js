/*
Name: GURPRIYA KAUR
FILE: question1.css (Formatting for index.html)
SUBMISSION: CRISTINA RIBEIRO
COURSE: WEB PROGRAMMING
*/

var questions = [                /* array containing all questions to be displayed on screen*/
{
"question":"In which element do we put the javascript in html?",
"option1":"html",
"option2":"body",
"option3":"script",
"option4":"head",
"answer":"3"
},
{
"question":"How many data types are there in javascript?",
"option1":"5",
"option2":"8",
"option3":"9",
"option4":"6",
"answer":"2"
},
{
"question":"How many loop statments are there in javascript ?",
"option1":"6",
"option2":"4",
"option3":"8",
"option4":"3",
"answer":"4"
}
];

var score = 0;                  /* initialising score*/
var currentQuestion = 0;        /* intitalising the question number*/
var totQuestions = questions.length;       /* assigning the number of questions into a variable*/
var container = document.getElementById('quizContainer');  /*returns the element from quizContainerontainer id in html and storing into a variable*/
var questionEl = document.getElementById("showQuestion");  /*returns the element from showQuestion id in html and storing into a variable*/ 
var opt1 = document.getElementById('opt1');               /*returns the element from opt1 id in html and storing into a variable*/
var opt2 = document.getElementById('opt2');               /*returns the element from opt2 id in html and storing into a variable*/
var opt3 = document.getElementById('opt3');               /*returns the element from opt3 id in html and storing into a variable*/
var opt4 = document.getElementById('opt4');               /*returns the element from opt4 id in html and storing into a variable*/
var nextButton = document.getElementById('nextButton');    /* gor the working of next button*/
var resultCont = document.getElementById('result');        
var answerSelected = 'green';                                /*if the answer is correct the background color will change to green */


function loadQuestion(questionNum)    /* function for loading question on screen and options as well*/
{
	 var q = questions[questionNum];
	 questionEl.innerHTML = q.question; 
	 opt1.textContent = q.option1;
	 opt2.textContent = q.option2;
	 opt3.textContent = q.option3;
	 opt4.textContent = q.option4;
};

function loadNextQuestion()  /*function for loading next question,changing bg colors, and displaying different messages on the basis of actions taken by user*/
{
    document.body.style.background = '#6069f0';
	container.style.display = '';
	document.getElementById("hlp").style.display = 'none';
	var selectedOption = document.querySelector('input[type=radio]:checked');
	if(!selectedOption) 
	{
		alert("Please select your answerSelected!");
	
		return;
	}
	
	answerSelected = selectedOption.value;
	
	if(questions[currentQuestion].answer == answerSelected)
	{
		score += 1;
	}
	
	selectedOption.checked = false;
	currentQuestion++;
	
	if(currentQuestion == totQuestions - 1)
	{
		nextButton.textContent = 'Finish';
		
	}
	if(currentQuestion == totQuestions)
	{
		container.style.display = 'none';
		resultCont.style.display = '';
		resultCont.textContent = 'Your total Score is: ' + score +' out of 3';  /*displaying th etotal score*/
 		return;
	}
	loadQuestion(currentQuestion);
}

function restartQuiz()         /*function for starting the quiz again*/
{
	nextButton.textContent = 'Next Question';
	resultCont.style.display = 'none';
	container.style.display = '';
	currentQuestion = 0;
	loadQuestion(currentQuestion);
	
	
}

function submit()          /*for the working of submit button*/
{
	  var selectedOption = document.querySelector('input[type=radio]:checked');
	  answerSelected = selectedOption.value;
	  if(questions[currentQuestion].answer == answerSelected)
	  {
		   document.body.style.background = "#27571b"; 
	        container.style.display = 'none';
			document.getElementById("hlp").style.display = '';
		document.getElementById("hlp").innerHTML = "Yippie!!!Your answer is correct.<br>Click on next question to proceed!!!";
	  }
	   else
	   {
		  document.body.style.background = "red";
		   container.style.display = 'none';
		  document.getElementById("hlp").style.display = '';
		document.getElementById("hlp").innerHTML = "OOPS!!!Your answer is wrong";
	   }
         
}

function help()      /*for the working of the help button*/
{
	 container.style.display = 'none';
	 document.getElementById("hlp").style.display = '';
     document.getElementById("cont").style.display = 'none';
	 document.getElementById("go-btn").style.display = '';
	 document.getElementById("nextButton").style.display = 'none';
	 document.getElementById("sub-btn").style.display = 'none';
	 document.getElementById("hlp").innerHTML = "You are playing through a simple<b> TRIVIA GAME !!!!</b><br><br>1) There are three questions and four options.<br><br>2) You have to select one correct option and click on submit answer.<br><br>3) If your answer is correct the background will get green otherwise red. Click on next question to proceed and restart to start the game again!!!";
}

function go()     /*function for the working of the go back button*/
{
	
	 container.style.display = '';
	 document.getElementById("cont").style.display = '';
     document.getElementById("hlp").style.display = 'none';
	 document.getElementById("go-btn").style.display = 'none';
	 document.getElementById("nextButton").style.display = '';
	 document.getElementById("sub-btn").style.display = '';
}

loadQuestion(currentQuestion);      /*loading the first question manually*/


	